COMPILER_FLAGS := \
	-shared \
	-fPIC \
	-Wno-unused-parameter \
	-g \
	-O0 \
	-std=c++11 \
	-stdlib=libstdc++ \
	-pthread \
	-m32

LIBRARIES := \
	-ldl \
	-lGL \
	-lftgl \
	-ldbus-1 \
	-ldbus-c++-1

INCLUDES := \
	-I/usr/include/dbus-c++-1/ \
	-I/usr/include/dbus-1.0 \
	-I/usr/include/dbus-1.0/dbus \
	-I./build/

SERVER_DIRECTORY := ./src/server/
SERVER_FILES := $(SERVER_DIRECTORY)extensions/*.cpp $(SERVER_DIRECTORY)/*.cpp

all: post-build

init:
	mkdir -p "./build"

introspection: init
	dbusxx-xml2cpp $(SERVER_DIRECTORY)server-introspection.xml --adaptor=./build/server-dbus-gen.h

shared: introspection
	clang++ $(SERVER_FILES) $(COMPILER_FLAGS) $(INCLUDES) $(LIBRARIES) -o ./build/tuxile.so

post-build: shared
	rm ./build/server-dbus-gen.h

clean:
	rm ./build/*
