var http = require("http");
var keysym = require("keysym");
var dbus = require("dbus-native");
var child_process = require("child_process");

var defaultTextDisplayTime = 5000;
var serviceName = "org.exilecraft.tuxile";
var objectPath = "/org/exilecraft/tuxile";
var interfaceName = "org.exilecraft.tuxile.ServerAdaptor";
var poeNinjaBaseApiUrl = "http://poe.ninja/api/Data/";
var poeNinjaLeagueQuery = "?league=Essence";
var sessionBus = dbus.sessionBus();

sessionBus.getService(serviceName).getInterface(objectPath, interfaceName, function(err, server)
{
	server.on("KeyPressed", function(keySym)
	{
		var keyName = keysym.fromKeysym(keySym).names[0];
		if (keyName == "F2") server.Type("\n/remaining\n");
		else if (keyName == "F3") server.Type("\n/hideout\n");
		else if (keyName == "F4")
		{
			server.DisplayText(displayDamagePerSecond());
			setTimeout(function() { server.DisplayText(""); }, defaultTextDisplayTime);
		}
		else if (keyName == "F6")
		{
			lookupPrice().then(function (s)
			{
				server.DisplayText(s);
				setTimeout(function() { server.DisplayText(""); }, defaultTextDisplayTime);
			});
		}
	});

	server.DisplayText("Tuxile client connected.", function (err)
	{
		setTimeout(function() { server.DisplayText(""); }, defaultTextDisplayTime);
	});
});

var getPoeNinjaResourceName = function (item)
{
	if (item.aps > 0) return "GetUniqueWeaponOverview";
	else if (/flask$/mig.test(item.baseType)) return "GetUniqueFlaskOverview";
	else if (/ring$/mig.test(item.baseType)) return "GetUniqueAccessoryOverview";
	else if (/amulet$/mig.test(item.baseType)) return "GetUniqueAccessoryOverview";
	else if (/belt$/mig.test(item.baseType)) return "GetUniqueAccessoryOverview";
	else if (/jewel$/mig.test(item.baseType)) return "GetUniqueJewelOverview";
	else if (/essence/mig.test(item.name)) return "GetEssenceOverview";
	else if (/card$/mig.test(item.rarity)) return "GetDivinationCardsOverview";
	return "GetUniqueArmourOverview";
};

var lookupPrice = function()
{
	return new Promise(function(resolve, reject)
	{
		var item = parseItem(getClipboardText());
		var resourceName = getPoeNinjaResourceName(item);
		var url = poeNinjaBaseApiUrl + resourceName + poeNinjaLeagueQuery;

		http.get(url, (res) =>
		{
			var rawData = "";
			res.on("data", function (chunk) { rawData += chunk });
			res.on("end", function()
			{
				var lines = JSON.parse(rawData).lines;
				var found = null;

				for (var i=0; i<lines.length; i++)
				{
					if (lines[i].name.indexOf(item.name) != -1)
					{
						found = lines[i];
					}
				}

				if (found) resolve(item.name + " - " + found.chaosValue + "c");
				else resolve(item.name + " not found.");
			});
		});
	});
};

var displayDamagePerSecond = function()
{
	var item = parseItem(getClipboardText());

	var output = "";
	output += "Total:     " + item.dps.total + "\n";
	output += "Physical:  " + item.dps.physical + "\n";
	output += "Fire:      " + item.dps.fire + "\n";
	output += "Cold:      " + item.dps.cold + "\n";
	output += "Lightning: " + item.dps.lightning + "\n";
	output += "Elemental: " + item.dps.elemental;
	return output;
};

var getClipboardText = function()
{
	return child_process.spawnSync("xclip", ["-o"]).stdout.toString();
};

var parseIntRange = function(rangeRegex, text)
{
	var matches = rangeRegex.exec(text);
	if (!matches) return createRangeObject(0, 0);
	var min = parseInt(matches[1]) || 0;
	var max = parseInt(matches[2]) || 0;
	return createRangeObject(min, max);
};

var createRange = function(min, max)
{
	return {
		"min": min,
		"max": max,
		"dps": function(aps) { return (((min + max) / 2) * aps).toFixed(); },
		"add": function (x) { return createRange(min + x.min, max + x.max); }
	};
};

var createRangeFromText = function(regex, itemText)
{
	var matches = regex.exec(itemText);
	if (!matches) return createRange(0, 0);
	return createRange(parseInt(matches[1]), parseInt(matches[2]));
}

var parseAttacksPerSecond = function(itemText)
{
	var parsedAps = /Attacks per Second: ([^\s]+).*$/mig.exec(itemText);
	return parsedAps ? parseFloat(parsedAps[1]) : 0;
};

var parseItem = function(itemText)
{
	var rarity = /Rarity: (.*)$/mig.exec(itemText)[1];
	var name = itemText.split("\n")[1];
	var baseType = itemText.split("\n")[2];
	var aps = parseAttacksPerSecond(itemText);

	var damageRanges =
	{
		physical: createRangeFromText(/Physical Damage: (\d+)-(\d+)/ig, itemText),
		fire: createRangeFromText(/Adds (\d+) to (\d+) Fire Damage(?!.*spells)/ig, itemText),
		cold: createRangeFromText(/Adds (\d+) to (\d+) Cold Damage(?!.*spells)/ig, itemText),
		lightning: createRangeFromText(/Adds (\d+) to (\d+) Lightning Damage(?!.*spells)/ig, itemText)
	};

	damageRanges.elemental = damageRanges.fire.add(damageRanges.cold).add(damageRanges.lightning);
	damageRanges.total = damageRanges.elemental.add(damageRanges.physical);

	var item =
	{
		rarity: rarity,
		name: name,
		baseType: baseType,
		damage: damageRanges,
		aps: aps,
		dps:
		{
			physical: damageRanges.physical.dps(aps),
			fire: damageRanges.fire.dps(aps),
			cold: damageRanges.cold.dps(aps),
			lightning: damageRanges.lightning.dps(aps),
			elemental: damageRanges.elemental.dps(aps),
			total: damageRanges.total.dps(aps),
		}
	};

	return item;
};

