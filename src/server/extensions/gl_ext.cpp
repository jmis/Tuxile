#include "../tuxile.h"

namespace tuxile
{
	namespace gl_ext
	{
		int getScreenHeight()
		{
			GLint viewport[4];
			glGetIntegerv(GL_VIEWPORT, viewport);
			return viewport[3];
		}

		void renderText(const char* textToDisplay)
		{
			static FTFont* font = NULL;

			if (font == NULL)
			{
				font = new FTPixmapFont(FONT_PATH);
				font->FaceSize(24);
			}

			auto lines = string_ext::split(std::string(textToDisplay), '\n');
			auto currentLine = 1;
			auto screenHeight = getScreenHeight();

			for (auto line : lines)
			{
				auto currentY = screenHeight + (currentLine * -font->LineHeight());
				font->Render(line.c_str(), -1, FTPoint(100, currentY));
				currentLine++;
			}
		}
	}
}
