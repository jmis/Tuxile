#include "../tuxile.h"

namespace tuxile
{
	namespace x_ext
	{
		XKeyEvent createKeyEvent(Display *display, Window &win, Window &winRoot, bool press, char c, int modifiers)
		{
			KeyCode keyCode = XKeysymToKeycode(display, (KeyCode) c == '\n' ? XK_Return : c);
			XKeyEvent event;
			event.display     = display;
			event.window      = win;
			event.root        = winRoot;
			event.subwindow   = None;
			event.time        = CurrentTime;
			event.x           = 1;
			event.y           = 1;
			event.x_root      = 1;
			event.y_root      = 1;
			event.same_screen = True;
			event.keycode     = keyCode;
			event.state       = modifiers;
			event.type = press ? KeyPress : KeyRelease;
			return event;
		}

		void type(Display* display, std::string text)
		{
			auto winRoot = XDefaultRootWindow(display);
			Window winFocus;
			int revert;
			XGetInputFocus(display, &winFocus, &revert);

			for (auto c : text)
			{
				XKeyEvent event = createKeyEvent(display, winFocus, winRoot, true, c, 0);
				XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);

				event = createKeyEvent(display, winFocus, winRoot, false, c, 0);
				XSendEvent(event.display, event.window, True, KeyReleaseMask, (XEvent *)&event);
			}
		}
	}
}

