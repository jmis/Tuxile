#include "../tuxile.h"

namespace tuxile
{
	namespace string_ext
	{
		std::vector<std::string> split(const std::string &s, char delim)
		{
			std::vector<std::string> elems;
			std::stringstream ss;
			ss.str(s);
			std::string item;
			while (std::getline(ss, item, delim)) elems.push_back(item);
			return elems;
		}

		const char* to_const(const std::string &s)
		{
			auto writable = new char[s.size() + 1];
			std::copy(s.begin(), s.end(), writable);
			writable[s.size()] = '\0';
			return writable;
		}
	}
}