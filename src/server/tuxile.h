#ifndef __TUXILE_H
#define __TUXILE_H

#define DBUS_HAS_RECURSIVE_MUTEX
#define GL_GLEXT_PROTOTYPES

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <thread>
#include <dlfcn.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <FTGL/ftgl.h>
#include <dbus-c++/dbus.h>
#include <X11/X.h>

#include "server-dbus-gen.h"

#define TUXILE_VERSION "1.0.0"
#define DBUS_SERVER_NAME "org.exilecraft.tuxile"
#define DBUS_SERVER_PATH "/org/exilecraft/tuxile"
#define FONT_PATH "/usr/share/fonts/truetype/ubuntu-font-family/UbuntuMono-R.ttf"

namespace tuxile
{
	class Server : public org::exilecraft::tuxile::ServerAdaptor_adaptor, public DBus::IntrospectableAdaptor, public DBus::ObjectAdaptor
	{
		public:
			Server(DBus::Connection &connection);
			void DisplayText(const std::string &name);
			void HandleFrame();
			void Type(const std::string &text);
			void StartXServerThread(Display* display);
			void StartDBUSThread();
			static Server* Start();
		private:
			void XServerEventLoop();
			const char* textToDisplay;
			Display* display;
			std::vector<std::thread>* threads;
	};

	namespace string_ext
	{
		std::vector<std::string> split(const std::string &s, char delim);
		const char* to_const(const std::string &s);
	}

	namespace gl_ext
	{
		void renderText(const char* textToDisplay);
	}

	namespace x_ext
	{
		void type(Display* display, std::string text);
	}
}

#endif