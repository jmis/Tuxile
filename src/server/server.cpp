#include "tuxile.h"

namespace tuxile
{
	Server::Server(DBus::Connection &connection) : DBus::ObjectAdaptor(connection, DBUS_SERVER_PATH)
	{
		threads = new std::vector<std::thread>();
		textToDisplay = "Tuxile server loaded successfully\nVersion 1.0.0\nWaiting for client DBUS connection...";
	}

	void Server::DisplayText(const std::string &text)
	{
		textToDisplay = string_ext::to_const(text);
	}

	void Server::Type(const std::string &text)
	{
		x_ext::type(display, text);
	}

	void Server::HandleFrame()
	{
		if (textToDisplay == NULL) return;
		gl_ext::renderText(textToDisplay);
	}

	void Server::StartDBUSThread()
	{
		threads->push_back(std::thread([]() { DBus::default_dispatcher->enter(); }));
	}

	void Server::StartXServerThread(Display* display)
	{
		this->display = display;
		threads->push_back(std::thread([this]() { this->XServerEventLoop(); }));
	}

	void Server::XServerEventLoop()
	{
		Window root = DefaultRootWindow(display);
		Window curFocus;
		char buf[17];
		KeySym ks;
		XComposeStatus comp;
		XEvent ev;
		int revert;

		XGetInputFocus (display, &curFocus, &revert);
		XSelectInput(display, curFocus, KeyPressMask);

		while (1)
		{
			while (XEventsQueued(display, QueuedAlready))
			{
				XNextEvent(display, &ev);
				if (ev.type == KeyPress)
				{
					KeySym keysym;
					char keyChar[1] = { 'x' };
					XLookupString(&ev.xkey, keyChar, 1, &keysym, NULL);
					this->KeyPressed(keysym);
				}
			}
		}
	}

	Server* Server::Start()
	{
		DBus::default_dispatcher = new DBus::BusDispatcher();
		DBus::Connection connection = DBus::Connection::SessionBus();
		connection.request_name(DBUS_SERVER_NAME);
		return new Server(connection);
	}
}
