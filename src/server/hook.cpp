#include "tuxile.h"

using namespace tuxile;

typedef void* (*real_dlsym_def)(void* handle, const char* symbol);
typedef __GLXextFuncPtr (*real_glXGetProcAddressARB_def)(const GLubyte* arg);
typedef void (*real_glXSwapBuffers_def)(Display* dpy, GLXDrawable drawable);

static Server* server = 0;
static real_glXGetProcAddressARB_def real_glXGetProcAddressARB;
static real_dlsym_def real_dlsym;
static real_glXSwapBuffers_def real_glXSwapBuffers;

void handleDynamicLibraryLoadError()
{
	auto errorMessage = dlerror();

	if (errorMessage != NULL)
	{
		fprintf(stderr, "Failed to load library: %s\n", errorMessage);
		exit(1);
	}
}

void glXSwapBuffers(Display* display, GLXDrawable drawable)
{
	if (!real_glXSwapBuffers)
	{
		real_glXSwapBuffers = (real_glXSwapBuffers_def) real_dlsym(RTLD_NEXT, "glXSwapBuffers");
		handleDynamicLibraryLoadError();

		server = Server::Start();
		server->StartDBUSThread();
		server->StartXServerThread(display);
	}

	server->HandleFrame();
	real_glXSwapBuffers(display, drawable);
}

__GLXextFuncPtr glXGetProcAddressARB(const GLubyte* arg)
{
	if (!real_glXGetProcAddressARB)
	{
		real_glXGetProcAddressARB = (real_glXGetProcAddressARB_def) real_dlsym(RTLD_NEXT, "glXGetProcAddressARB");
		handleDynamicLibraryLoadError();
	}

	if (strcmp((char*) arg, "glXSwapBuffers") == 0)	return (__GLXextFuncPtr) glXSwapBuffers;
	return real_glXGetProcAddressARB(arg);
}

void* dlsym(void* handle, const char* symbol)
{
	if (!real_dlsym)
	{
		real_dlsym = (real_dlsym_def) dlvsym(RTLD_DEFAULT, "dlsym", "GLIBC_2.0");
		handleDynamicLibraryLoadError();
	}

	if (strcmp(symbol, "glXGetProcAddressARB") == 0) return (void*) glXGetProcAddressARB;
	return real_dlsym(handle, symbol);
}